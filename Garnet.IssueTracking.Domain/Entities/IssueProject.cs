﻿using Garnet.Infrastructure.Repositories;

namespace Garnet.IssueTracking.Domain.Entities
{
    public class IssueProject : IHasId<string>
    {
        public IssueProject(string projectId, string title)
        {
            Id = projectId;
            Title = title;
        }

        public string Id { get; }

        public string Title { get; }
    }
}
