﻿using Garnet.Infrastructure.Repositories;

namespace Garnet.IssueTracking.Domain.Entities
{
    public struct IssueTracker : IHasId<int>
    {
        public IssueTracker(int id, string title)
        {
            Id = id;
            Title = title;
        }

        public int Id { get; }

        public string Title { get; }

        public bool IsEmpty
        {
            get { return Id <= 0 || string.IsNullOrEmpty(Title); }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return ((IssueTracker)obj).Id == Id;
        }

        public override int GetHashCode()
        {
            unchecked { return Id * 397; }
        }
    }
}
