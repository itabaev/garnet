﻿using Garnet.Infrastructure.Repositories;
using System;

namespace Garnet.IssueTracking.Domain.Entities
{
    public class IssueAuthor : IHasId<Guid>
    {
        public IssueAuthor(Guid personId, string firstName, string lastName)
        {
            Id = personId;
            FirstName = firstName;
            LastName = lastName;
        }

        public Guid Id { get; }

        public string FirstName { get; }

        public string LastName { get; }
    }
}
