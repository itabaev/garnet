﻿using Garnet.IssueTracking.Domain.Values;
using System;

namespace Garnet.IssueTracking.Domain.Entities
{
    public class IssueHistory
    {
        public IssueHistory(DateTime date, IssueHistoryType historyType, object oldValue, object newValue)
        {
            Date = date;
            HistoryType = historyType;
            OldValue = oldValue?.ToString();
            NewValue = newValue?.ToString();
        }

        public IssueHistory(IssueHistoryType historyType, object oldValue, object newValue)
            : this(DateTime.UtcNow, historyType, oldValue, newValue)
        {
        }

        public DateTime Date { get; }

        public IssueHistoryType HistoryType { get; }

        public string OldValue { get; }

        public string NewValue { get; }
    }
}
