﻿namespace Garnet.IssueTracking.Domain.Values
{
    public enum IssuePriority
    {
        Normal,
        Low,
        High,
        Urgent
    }
}