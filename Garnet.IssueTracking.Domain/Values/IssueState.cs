﻿namespace Garnet.IssueTracking.Domain.Values
{
    public enum IssueState
    {
        Open,
        Feedback,
        Paused,
        Pending,
        Resolved,
        Closed
    }
}