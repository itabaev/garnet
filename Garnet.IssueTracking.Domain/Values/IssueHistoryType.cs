﻿namespace Garnet.IssueTracking.Domain.Values
{
    public enum IssueHistoryType
    {
        Title,
        Description,
        Tracker,
        Priority,
        State
    }
}
