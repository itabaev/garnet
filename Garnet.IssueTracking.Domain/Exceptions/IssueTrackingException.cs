﻿using System;
using Garnet.Infrastructure.Exceptions;

namespace Garnet.IssueTracking.Domain.Exceptions
{
    public abstract class IssueTrackingException : DomainException
    {
        public IssueTrackingException()
        {
        }

        public IssueTrackingException(string message) : base(message)
        {
        }

        public IssueTrackingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
