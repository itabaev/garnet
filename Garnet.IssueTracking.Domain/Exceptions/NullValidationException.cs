﻿using System;

namespace Garnet.IssueTracking.Domain.Exceptions
{
    public class NullValidationException : ValidationException
    {
        public NullValidationException(string paramName)
            : this(paramName, $"The value of field '{paramName}' is null")
        {
        }

        public NullValidationException(string paramName, string message)
            : this(paramName, message, null)
        {
        }

        public NullValidationException(string paramName, string message, Exception innerException)
            : base(paramName, message, innerException)
        {
        }
    }
}
