﻿using System;

namespace Garnet.IssueTracking.Domain.Exceptions
{
    public class ValidationException : IssueTrackingException
    {
        public ValidationException(string paramName, string message)
            : this(paramName, message, null)
        {
        }

        public ValidationException(string paramName, string message, Exception innerException)
            : base(message, innerException)
        {
            ParamName = paramName;
        }

        public string ParamName { get; }
    }
}
