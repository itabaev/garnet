﻿using Garnet.Infrastructure.Repositories;
using Garnet.IssueTracking.Domain.Aggregates;
using Garnet.IssueTracking.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Garnet.IssueTracking.Domain.Repositories
{
    public interface IIssueRepository : IRepository<Issue, long>
    {
        Task<IEnumerable<IssueTracker>> GetTrackers();

        Task<IssueTracker> GetTracker(int trackerId);

        Task AddHistory(long issueId, IssueHistory history);

        Task<int> AddComment(long issueId, IssueComment comment);
    }
}