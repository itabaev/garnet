﻿using System;
using Garnet.Infrastructure.Repositories;
using Garnet.IssueTracking.Domain.Entities;

namespace Garnet.IssueTracking.Domain.Aggregates
{
    public class IssueComment : IAggregate<int>
    {
        public IssueComment(int commentId, string text, DateTime createDate, IssueAuthor author)
        {
            Id = commentId;
            Text = text;
            CreateDate = createDate;
            Author = author;
        }

        public int Id { get; }

        public string Text { get; }

        public DateTime CreateDate { get; }

        public IssueAuthor Author { get; }
    }
}
