﻿using System;
using System.Collections.Generic;
using System.Linq;
using Garnet.Infrastructure.Repositories;
using Garnet.IssueTracking.Domain.Entities;
using Garnet.IssueTracking.Domain.Exceptions;
using Garnet.IssueTracking.Domain.Values;
using JetBrains.Annotations;

namespace Garnet.IssueTracking.Domain.Aggregates
{
    public class Issue : IAggregate<long>
    {
        private readonly List<IssueHistory> _history;
        private readonly List<IssueComment> _comments;

        public Issue(long issueId, DateTime createDate, string title, string description, IssueTracker tracker, IssuePriority priority, IssueState state, IssueProject project, IssueAuthor author, IEnumerable<IssueHistory> history)
        {
            if (string.IsNullOrWhiteSpace(title))
                throw new ArgumentNullException(nameof(title));
            if (string.IsNullOrWhiteSpace(description))
                throw new ArgumentNullException(nameof(description));
            if (project == null)
                throw new ArgumentNullException(nameof(project));
            if (author == null)
                throw new ArgumentNullException(nameof(author));

            Id = issueId;
            CreateDate = createDate;
            Title = title;
            Description = description;
            Tracker = tracker;
            Priority = priority;
            State = state;
            Project = project;
            Author = author;
            _history = new List<IssueHistory>(history);
            _comments = new List<IssueComment>();
        }

        public long Id { get; }

        public DateTime CreateDate { get; }

        [NotNull]
        public string Title { get; private set; }

        [NotNull]
        public string Description { get; private set; }

        public IssueTracker Tracker { get; private set; }

        public IssuePriority Priority { get; private set; }

        public IssueState State { get; private set; }

        [NotNull]
        public IssueProject Project { get; }

        [NotNull]
        public IssueAuthor Author { get; }

        [NotNull]
        public IReadOnlyCollection<IssueHistory> History { get => _history.AsReadOnly(); }

        [NotNull]
        public IReadOnlyCollection<IssueComment> Comments { get => _comments.AsReadOnly(); }

        private void AddHistory<T>(IssueHistoryType historyType, T oldValue, T newValue)
        {
            _history.Add(new IssueHistory(historyType, oldValue, newValue));
        }

        public void ChangeTitle([NotNull]string title)
        {
            if (string.IsNullOrWhiteSpace(title))
                throw new NullValidationException(nameof(title));

            if (string.Equals(title, Title, StringComparison.InvariantCulture))
                return;

            var oldTitle = Title;
            Title = title;
            AddHistory(IssueHistoryType.Title, oldTitle, title);
        }

        public void ChangeDescription([NotNull]string description)
        {
            if (string.IsNullOrWhiteSpace(description))
                throw new NullValidationException(nameof(description));

            if (string.Equals(description, Description, StringComparison.InvariantCulture))
                return;

            var oldDescription = Description;
            Description = description;
            AddHistory(IssueHistoryType.Description, oldDescription, description);
        }

        public void ChangeTracker([NotNull]IssueTracker tracker)
        {
            if (tracker.IsEmpty)
                throw new NullValidationException(nameof(tracker));

            if (Equals(tracker, Tracker))
                return;

            var oldTrackerId = Tracker.Id;
            Tracker = tracker;
            AddHistory(IssueHistoryType.Tracker, oldTrackerId, Tracker.Id);
        }

        public void ChangePriority(IssuePriority priority)
        {
            if (priority == Priority)
                return;

            var oldPriority = Priority;
            Priority = priority;
            AddHistory(IssueHistoryType.Priority, oldPriority, priority);
        }

        public void ChangeState(IssueState state)
        {
            if (state == State)
                return;

            var oldState = State;
            State = state;
            AddHistory(IssueHistoryType.State, oldState, state);
        }

        public void AddComment(IssueComment comment)
        {
            if (comment == null)
                throw new NullValidationException(nameof(comment));

            var comments = _comments.AsReadOnly();
            if (comments.Any(c => c.CreateDate >= comment.CreateDate.AddMinutes(1)))
                throw new ValidationException(nameof(comment.CreateDate), "Сan not add comments in hindsight");

            _comments.Add(comment);
        }
    }
}
