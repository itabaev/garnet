﻿using System.Data;
using Garnet.Infrastructure.SQL.Session;
using Npgsql;

namespace Garnet.Infrastructure.PostgreSQL.Session
{
    public class SqlSession : ISqlSession
    {
        private readonly string _connectionString;
        private IDbConnection _connection;

        public SqlSession(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDbConnection GetConnection()
        {
            if (_connection != null)
                return _connection;

            _connection = new NpgsqlConnection(_connectionString);
            _connection.Open();
            return _connection;
        }

        public void Dispose()
        {
            _connection?.Dispose();
        }
    }
}
