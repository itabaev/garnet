﻿using Garnet.Infrastructure.SQL.Session;

namespace Garnet.Infrastructure.PostgreSQL.Session
{
    public class SqlSessionFactory : ISqlSessionFactory
    {
        private readonly string _connectionString;

        public SqlSessionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ISqlSession Create()
        {
            return new SqlSession(_connectionString);
        }
    }
}
