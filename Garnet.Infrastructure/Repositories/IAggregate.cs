﻿namespace Garnet.Infrastructure.Repositories
{
    public interface IAggregate
    {
    }

    public interface IAggregate<TId> : IAggregate, IHasId<TId>
        where TId: struct
    {
    }
}
