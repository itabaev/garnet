﻿using System.Threading.Tasks;

namespace Garnet.Infrastructure.Repositories
{
    public interface IRepository
    {
    }

    public interface IRepository<TAggregate> : IRepository
        where TAggregate : IAggregate
    {
        Task Update(TAggregate aggregate);
    }

    public interface IRepository<TAggregate, TId> : IRepository<TAggregate>
        where TAggregate : IAggregate<TId>
        where TId: struct
    {
        Task<TAggregate> Get(TId id);

        Task<TId> Create(TAggregate aggregate);

        Task Delete(TId id);
    }
}
