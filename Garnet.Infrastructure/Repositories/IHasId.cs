﻿namespace Garnet.Infrastructure.Repositories
{
    public interface IHasId<TId>
    {
        TId Id { get; }
    }
}
