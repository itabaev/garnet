﻿namespace Garnet.Infrastructure.Repositories
{
    public interface IRepositoryFactory
    {
        IRepository<TAggregate> Create<TAggregate>()
            where TAggregate : IAggregate;

        IRepository<TAggregate, TId> Create<TAggregate, TId>()
            where TAggregate : IAggregate<TId>
            where TId: struct;
    }
}
