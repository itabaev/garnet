﻿namespace Garnet.Infrastructure.Commands
{
    public interface ICommandHandlerFactory
    {
        ICommandHandler<TCommand> Create<TCommand>()
            where TCommand : ICommand;

        ICommandHandler<TCommand, TResult> Create<TCommand, TResult>()
            where TCommand : ICommand;
    }
}
