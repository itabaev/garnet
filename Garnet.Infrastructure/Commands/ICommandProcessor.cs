﻿using System.Threading.Tasks;

namespace Garnet.Infrastructure.Commands
{
    public interface ICommandProcessor
    {
        Task Handle<TCommand>(TCommand command)
            where TCommand : ICommand;

        Task<TResult> Handle<TCommand, TResult>(TCommand command)
            where TCommand : ICommand;
    }
}
