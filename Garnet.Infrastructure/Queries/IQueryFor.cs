﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Garnet.Infrastructure.Queries
{
    public interface IQueryFor<T>
    {
        Task<IEnumerable<T>> All();

        Task<IEnumerable<T>> AllWith<TCriterion>(TCriterion criterion) where TCriterion : ICriterion;

        Task<T> OneWith<TCriterion>(TCriterion criterion) where TCriterion : ICriterion;

        Task<T> One<TId>(TId id) where TId : struct;
    }
}