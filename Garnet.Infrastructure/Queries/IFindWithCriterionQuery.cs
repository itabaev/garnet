﻿using System.Threading.Tasks;

namespace Garnet.Infrastructure.Queries
{
    public interface IFindWithCriterionQuery<in TCriterion, TResult> : IQuery<TResult>
        where TCriterion : ICriterion
    {
        Task<TResult> Execute(TCriterion criterion);
    }
}