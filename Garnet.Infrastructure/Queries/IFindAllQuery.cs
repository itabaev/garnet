﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Garnet.Infrastructure.Queries
{
    public interface IFindAllQuery<TResult> : IQuery<TResult>
    {
        Task<IEnumerable<TResult>> Execute();
    }
}