﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Garnet.Infrastructure.Queries
{
    public interface IFindAllWithCriterionQuery<in TCriterion, TResult> : IQuery<TResult>
        where TCriterion : ICriterion
    {
        Task<IEnumerable<TResult>> Execute(TCriterion criterion);
    }
}