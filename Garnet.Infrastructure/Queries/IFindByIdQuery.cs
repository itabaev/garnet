﻿using System.Threading.Tasks;

namespace Garnet.Infrastructure.Queries
{
    public interface IFindByIdQuery<in TId, TResult> : IQuery<TResult>
        where TId : struct
    {
        Task<TResult> Execute(TId id);
    }
}