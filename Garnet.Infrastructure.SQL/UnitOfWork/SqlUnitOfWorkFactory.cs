﻿using Garnet.Infrastructure.SQL.Session;
using Garnet.Infrastructure.UnitOfWork;

namespace Garnet.Infrastructure.SQL.UnitOfWork
{
    public class SqlUnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly ISqlSession _session;

        public SqlUnitOfWorkFactory(ISqlSession session)
        {
            _session = session;
        }

        public IUnitOfWork Create()
        {
            var unitOfWork = new SqlUnitOfWork(_session);
            return unitOfWork;
        }
    }
}
