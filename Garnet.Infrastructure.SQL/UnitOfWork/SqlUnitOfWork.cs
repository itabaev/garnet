﻿using System.Data;
using Garnet.Infrastructure.SQL.Session;
using Garnet.Infrastructure.UnitOfWork;

namespace Garnet.Infrastructure.SQL.UnitOfWork
{
    public class SqlUnitOfWork : IUnitOfWork
    {
        private readonly IDbTransaction _transaction;

        private bool _commited;

        public SqlUnitOfWork(ISqlSession session, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var connection = session.GetConnection();
            _transaction = connection.BeginTransaction(isolationLevel);
        }

        public void Commit()
        {
            _transaction.Commit();
            _commited = true;
        }

        public void Dispose()
        {
            if (!_commited)
            {
                _transaction.Rollback();
            }
            _transaction.Dispose();
        }
    }
}
