﻿using System;
using System.Data;

namespace Garnet.Infrastructure.SQL.Session
{
    public interface ISqlSession : IDisposable
    {
        IDbConnection GetConnection();
    }
}