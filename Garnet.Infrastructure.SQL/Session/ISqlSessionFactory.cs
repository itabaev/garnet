﻿namespace Garnet.Infrastructure.SQL.Session
{
    public interface ISqlSessionFactory
    {
        ISqlSession Create();
    }
}