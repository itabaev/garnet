﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Garnet.Infrastructure.DataAccess;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.DataAccess.Params;
using Garnet.IssueTracking.Domain.Aggregates;
using Garnet.IssueTracking.Domain.Entities;
using Garnet.IssueTracking.Domain.Repositories;
using Garnet.IssueTracking.Domain.Values;

namespace Garnet.IssueTracking.Domain.Dapper.Repositories
{
    public class IssueRepository : IIssueRepository
    {
        private readonly IDataAccessService<IssueDto, long> _getIssueService;
        private readonly IDataAccessService<long, CreateIssueParams> _createIssueService;
        private readonly IDataAccessService<bool, UpdateIssueParams> _updateIssueService;
        private readonly IDataAccessService<bool, DeleteIssueParams> _deleteIssueService;
        private readonly IDataAccessService<IEnumerable<TrackerDto>> _getTrackersService;
        private readonly IDataAccessService<IEnumerable<HistoryDto>, long> _getHistoryService;
        private readonly IDataAccessService<bool, AddHistoryParams> _addHistoryService;
        private readonly IDataAccessService<int, AddCommentParams> _addCommentService;

        public IssueRepository(IDataAccessService<IssueDto, long> getIssueService,
            IDataAccessService<long, CreateIssueParams> createIssueService,
            IDataAccessService<bool, UpdateIssueParams> updateIssueService,
            IDataAccessService<bool, DeleteIssueParams> deleteIssueService,
            IDataAccessService<IEnumerable<TrackerDto>> getTrackersService,
            IDataAccessService<IEnumerable<HistoryDto>, long> getHistoryService,
            IDataAccessService<bool, AddHistoryParams> addHistoryService,
            IDataAccessService<int, AddCommentParams> addCommentService)
        {
            _getIssueService = getIssueService;
            _createIssueService = createIssueService;
            _updateIssueService = updateIssueService;
            _deleteIssueService = deleteIssueService;
            _getTrackersService = getTrackersService;
            _getHistoryService = getHistoryService;
            _addHistoryService = addHistoryService;
            _addCommentService = addCommentService;
        }

        public async Task<Issue> Get(long issueId)
        {
            var issue = await _getIssueService.Execute(issueId);
            if (issue == null)
                throw new ArgumentException("Issue not found", nameof(issueId));

            var history = await GetHistory(issueId);

            return new Issue(
                issue.IssueId,
                issue.CreateDate,
                issue.Title,
                issue.Description,
                new IssueTracker(issue.TrackerId, issue.TrackerTitle),
                Enum.Parse<IssuePriority>(issue.Priority, true),
                Enum.Parse<IssueState>(issue.State, true),
                new IssueProject(null, null),
                new IssueAuthor(Guid.Empty, null, null),
                history);
        }

        private async Task<IEnumerable<IssueHistory>> GetHistory(long issueId)
        {
            var history = await _getHistoryService.Execute(issueId);

            return history
                .Select(h =>
                    new IssueHistory(
                        h.Date,
                        Enum.Parse<IssueHistoryType>(h.HistoryType, true),
                        h.OldValue,
                        h.NewValue))
                .ToList();
        }

        public async Task<long> Create(Issue issue)
        {
            var issueId = await _createIssueService.Execute(new CreateIssueParams(
                issue.Title,
                issue.Description,
                issue.Tracker.Id,
                issue.Priority.ToString(),
                issue.State.ToString(),
                issue.Project.Id,
                issue.Author.Id
            ));
            return issueId;
        }

        public async Task Update(Issue issue)
        {
            await _updateIssueService.Execute(new UpdateIssueParams(
                issue.Id,
                issue.Title,
                issue.Description,
                issue.Tracker.Id,
                issue.Priority.ToString(),
                issue.State.ToString()
            ));
        }

        public async Task Delete(long issueId)
        {
            await _deleteIssueService.Execute(new DeleteIssueParams(issueId));
        }

        public async Task<IEnumerable<IssueTracker>> GetTrackers()
        {
            var trackers = await _getTrackersService.Execute();
            return trackers
                .Select(t => new IssueTracker(t.TrackerId, t.Title))
                .ToList();
        }

        public async Task<IssueTracker> GetTracker(int trackerId)
        {
            var trackers = await GetTrackers();
            var tracker = trackers.FirstOrDefault(t => t.Id == trackerId);
            return tracker;
        }

        public async Task AddHistory(long issueId, IssueHistory history)
        {
            await _addHistoryService.Execute(new AddHistoryParams(
                issueId,
                history.HistoryType.ToString(),
                history.OldValue,
                history.NewValue
            ));
        }

        public async Task<int> AddComment(long issueId, IssueComment comment)
        {
            var commentId = await _addCommentService.Execute(new AddCommentParams(
                issueId,
                comment.Text
            ));
            return commentId;
        }
    }
}
