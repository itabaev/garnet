﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.Queries;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.DataAccess.Params;
using Garnet.IssueTracking.Operations.Queries.Criteria;
using Garnet.IssueTracking.Operations.Queries.Models;

namespace Garnet.IssueTracking.Operations.Queries
{
    public class GetPagedIssuesQuery : IFindAllWithCriterionQuery<FindPagedIssuesCriterion, IssueModel>
    {
        private readonly IDataAccessService<IEnumerable<IssueDto>, GetPagedIssuesParams> _getPagedIssuesParams;

        public GetPagedIssuesQuery(IDataAccessService<IEnumerable<IssueDto>, GetPagedIssuesParams> getPagedIssuesParams)
        {
            _getPagedIssuesParams = getPagedIssuesParams;
        }

        public async Task<IEnumerable<IssueModel>> Execute(FindPagedIssuesCriterion criterion)
        {
            var issues = await _getPagedIssuesParams.Execute(
                new GetPagedIssuesParams(
                    criterion.Offset,
                    criterion.Limit
                ));
            return issues
                .Select(issue =>
                    new IssueModel(
                        issue.IssueId,
                        issue.CreateDate,
                        issue.Title,
                        issue.Description,
                        new TrackerModel(issue.TrackerId, issue.TrackerTitle),
                        issue.Priority,
                        issue.State))
                .ToList();
        }
    }
}
