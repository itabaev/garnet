﻿using System.Threading.Tasks;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.Queries;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.Operations.Queries.Models;

namespace Garnet.IssueTracking.Operations.Queries
{
    public class GetIssueQuery : IFindByIdQuery<long, IssueModel>
    {
        private readonly IDataAccessService<IssueDto, long> _getIssueService;

        public GetIssueQuery(IDataAccessService<IssueDto, long> getIssueService)
        {
            _getIssueService = getIssueService;
        }

        public async Task<IssueModel> Execute(long issueId)
        {
            var issue = await _getIssueService.Execute(issueId);
            return new IssueModel(
                issue.IssueId,
                issue.CreateDate,
                issue.Title,
                issue.Description,
                new TrackerModel(issue.TrackerId, issue.TrackerTitle),
                issue.Priority,
                issue.State);
        }
    }
}
