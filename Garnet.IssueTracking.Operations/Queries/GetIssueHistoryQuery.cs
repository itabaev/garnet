﻿using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.Queries;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.Domain.Values;
using Garnet.IssueTracking.Operations.Queries.Criteria;
using Garnet.IssueTracking.Operations.Queries.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Garnet.IssueTracking.Operations.Queries
{
    public class GetIssueHistoryQuery : IFindAllWithCriterionQuery<FindByIssueIdCriterion, HistoryModel>
    {
        private readonly IDataAccessService<IEnumerable<HistoryDto>, long> _getHistoryService;

        public GetIssueHistoryQuery(IDataAccessService<IEnumerable<HistoryDto>, long> getHistoryService)
        {
            _getHistoryService = getHistoryService;
        }

        public async Task<IEnumerable<HistoryModel>> Execute(FindByIssueIdCriterion criterion)
        {
            var history = await _getHistoryService.Execute(criterion.IssueId);
            return history
                .Select(h =>
                    new HistoryModel(
                        h.Date,
                        Enum.Parse<IssueHistoryType>(h.HistoryType, true),
                        h.OldValue,
                        h.NewValue))
                .ToList();
        }
    }
}
