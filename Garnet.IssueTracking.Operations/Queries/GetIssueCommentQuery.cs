﻿using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.Queries;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.DataAccess.Params;
using Garnet.IssueTracking.Operations.Queries.Criteria;
using Garnet.IssueTracking.Operations.Queries.Models;
using System.Threading.Tasks;

namespace Garnet.IssueTracking.Operations.Queries
{
    public class GetIssueCommentQuery : IFindWithCriterionQuery<FindByCommentIdCriterion, CommentModel>
    {
        private readonly IDataAccessService<CommentDto, GetCommentParams> _getCommentService;

        public GetIssueCommentQuery(IDataAccessService<CommentDto, GetCommentParams> getCommentService)
        {
            _getCommentService = getCommentService;
        }

        public async Task<CommentModel> Execute(FindByCommentIdCriterion criterion)
        {
            var comment = await _getCommentService.Execute(
                new GetCommentParams(
                    criterion.IssueId,
                    criterion.CommentId
                ));
            return new CommentModel(
                comment.CommentId,
                comment.Text,
                comment.CreateDate);
        }
    }
}
