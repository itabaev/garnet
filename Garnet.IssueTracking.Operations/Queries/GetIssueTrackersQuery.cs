﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.Queries;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.Operations.Queries.Models;

namespace Garnet.IssueTracking.Operations.Queries
{
    public class GetIssueTrackersQuery : IFindAllQuery<TrackerModel>
    {
        private readonly IDataAccessService<IEnumerable<TrackerDto>> _getTrackersService;

        public GetIssueTrackersQuery(IDataAccessService<IEnumerable<TrackerDto>> getTrackersService)
        {
            _getTrackersService = getTrackersService;
        }

        public async Task<IEnumerable<TrackerModel>> Execute()
        {
            var trackers = await _getTrackersService.Execute();
            return trackers
                .Select(tracker =>
                    new TrackerModel(
                        tracker.TrackerId,
                        tracker.Title))
                .ToList();
        }
    }
}
