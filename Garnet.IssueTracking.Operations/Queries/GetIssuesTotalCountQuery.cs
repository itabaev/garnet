﻿using System.Threading.Tasks;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.Queries;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.Operations.Queries.Criteria;

namespace Garnet.IssueTracking.Operations.Queries
{
    public class GetIssuesTotalCountQuery : IFindWithCriterionQuery<FindIssuesCriterion, int>
    {
        private readonly IDataAccessService<IssuesCountDto> _getIssuesCountService;

        public GetIssuesTotalCountQuery(IDataAccessService<IssuesCountDto> getIssuesCountService)
        {
            _getIssuesCountService = getIssuesCountService;
        }

        public async Task<int> Execute(FindIssuesCriterion criterion)
        {
            var issuesCount = await _getIssuesCountService.Execute();
            return issuesCount.Count;
        }
    }
}
