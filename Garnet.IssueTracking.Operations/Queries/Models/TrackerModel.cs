﻿namespace Garnet.IssueTracking.Operations.Queries.Models
{
    public class TrackerModel
    {
        public TrackerModel(int trackerId, string title)
        {
            TrackerId = trackerId;
            Title = title;
        }

        public int TrackerId { get; }

        public string Title { get; }
    }
}
