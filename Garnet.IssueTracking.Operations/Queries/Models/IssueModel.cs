﻿using System;

namespace Garnet.IssueTracking.Operations.Queries.Models
{
    public class IssueModel
    {
        public IssueModel(long issueId, DateTime createDate, string title, string description, TrackerModel tracker, string priority, string state)
        {
            IssueId = issueId;
            CreateDate = createDate;
            Title = title;
            Description = description;
            Tracker = tracker;
            Priority = priority;
            State = state;
        }

        public long IssueId { get; }

        public DateTime CreateDate { get; }

        public string Title { get; }

        public string Description { get; }

        public TrackerModel Tracker { get; set; }

        public string Priority { get; }

        public string State { get; }
    }
}
