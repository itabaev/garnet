﻿using System;

namespace Garnet.IssueTracking.Operations.Queries.Models
{
    public class CommentModel
    {
        public CommentModel(long commentId, string text, DateTime createDate)
        {
            CommentId = commentId;
            Text = text;
            CreateDate = createDate;
        }

        public long CommentId { get; }

        public string Text { get; }

        public DateTime CreateDate { get; }
    }
}
