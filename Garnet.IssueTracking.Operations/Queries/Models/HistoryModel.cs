﻿using Garnet.IssueTracking.Domain.Values;
using System;

namespace Garnet.IssueTracking.Operations.Queries.Models
{
    public class HistoryModel
    {
        public HistoryModel(DateTime date, IssueHistoryType historyType, string oldValue, string newValue)
        {
            Date = date;
            HistoryType = historyType.ToString();
            OldValue = oldValue;
            NewValue = newValue;
        }

        public DateTime Date { get; }

        public string HistoryType { get; }

        public string OldValue { get; }

        public string NewValue { get; }
    }
}
