﻿using Garnet.Infrastructure.Queries;

namespace Garnet.IssueTracking.Operations.Queries.Criteria
{
    public class FindPagedIssuesCriterion : FindIssuesCriterion, ICriterion
    {
        public FindPagedIssuesCriterion(int offset, int limit)
        {
            Offset = offset;
            Limit = limit;
        }

        public int Offset { get; }

        public int Limit { get; }
    }
}
