﻿using Garnet.Infrastructure.Queries;

namespace Garnet.IssueTracking.Operations.Queries.Criteria
{
    public class FindByIssueIdCriterion : ICriterion
    {
        public FindByIssueIdCriterion(long issueId)
        {
            IssueId = issueId;
        }

        public long IssueId { get; }
    }
}
