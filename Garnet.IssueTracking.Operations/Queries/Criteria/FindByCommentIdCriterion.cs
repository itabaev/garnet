﻿namespace Garnet.IssueTracking.Operations.Queries.Criteria
{
    public class FindByCommentIdCriterion : FindByIssueIdCriterion
    {
        public FindByCommentIdCriterion(long issueId, int commentId)
            : base(issueId)
        {
            CommentId = commentId;
        }

        public int CommentId { get; }
    }
}
