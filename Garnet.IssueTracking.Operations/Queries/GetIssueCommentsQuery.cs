﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.Queries;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.Operations.Queries.Criteria;
using Garnet.IssueTracking.Operations.Queries.Models;

namespace Garnet.IssueTracking.Operations.Queries
{
    public class GetIssueCommentsQuery : IFindAllWithCriterionQuery<FindByIssueIdCriterion, CommentModel>
    {
        private readonly IDataAccessService<IEnumerable<CommentDto>, long> _getCommentsService;

        public GetIssueCommentsQuery(IDataAccessService<IEnumerable<CommentDto>, long> getCommentsService)
        {
            _getCommentsService = getCommentsService;
        }

        public async Task<IEnumerable<CommentModel>> Execute(FindByIssueIdCriterion criterion)
        {
            var comments = await _getCommentsService.Execute(criterion.IssueId);
            return comments
                .Select(c =>
                    new CommentModel(
                        c.CommentId,
                        c.Text,
                        c.CreateDate))
                .ToList();
        }
    }
}
