﻿using Garnet.Infrastructure.Commands;

namespace Garnet.IssueTracking.Operations.Commands
{
    public class AddCommentCommand : ICommand
    {
        public AddCommentCommand(long issueId, string text)
        {
            IssueId = issueId;
            Text = text;
        }

        public long IssueId { get; }

        public string Text { get; }
    }
}
