﻿using System;
using System.Threading.Tasks;
using Garnet.Infrastructure.Commands;
using Garnet.Infrastructure.UnitOfWork;
using Garnet.IssueTracking.Domain.Aggregates;
using Garnet.IssueTracking.Domain.Entities;
using Garnet.IssueTracking.Domain.Repositories;

namespace Garnet.IssueTracking.Operations.Commands
{
    public class AddCommentCommandHandler : ICommandHandler<AddCommentCommand, int>
    {
        private readonly IIssueRepository _issueRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public AddCommentCommandHandler(IIssueRepository issueRepository, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _issueRepository = issueRepository;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<int> Handle(AddCommentCommand command)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var issue = await _issueRepository.Get(command.IssueId);

                var comment = new IssueComment(0, command.Text, DateTime.UtcNow, new IssueAuthor(Guid.Empty, null, null));
                issue.AddComment(comment);

                var commentId = await _issueRepository.AddComment(issue.Id, comment);

                uow.Commit();

                return commentId;
            }
        }
    }
}
