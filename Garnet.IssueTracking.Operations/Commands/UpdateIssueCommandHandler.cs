﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Garnet.Infrastructure.Commands;
using Garnet.Infrastructure.UnitOfWork;
using Garnet.IssueTracking.Domain.Repositories;

namespace Garnet.IssueTracking.Operations.Commands
{
    public class UpdateIssueCommandHandler : ICommandHandler<UpdateIssueCommand>
    {
        private readonly IIssueRepository _issueRepository;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public UpdateIssueCommandHandler(IIssueRepository issueRepository, IUnitOfWorkFactory unitOfWorkFactory)
        {
            _issueRepository = issueRepository;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task Handle(UpdateIssueCommand command)
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var issue = await _issueRepository.Get(command.IssueId);

                var date = DateTime.UtcNow;

                issue.ChangeTitle(command.Title);
                issue.ChangeDescription(command.Description);
                issue.ChangePriority(command.Priority);

                var tracker = await _issueRepository.GetTracker(command.TrackerId);
                issue.ChangeTracker(tracker);

                await _issueRepository.Update(issue);

                var histories = issue.History.Where(x => x.Date >= date);
                foreach (var history in histories)
                {
                    await _issueRepository.AddHistory(issue.Id, history);
                }

                uow.Commit();
            }
        }
    }
}
