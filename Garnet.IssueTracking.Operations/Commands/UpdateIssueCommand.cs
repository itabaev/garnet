﻿using Garnet.Infrastructure.Commands;
using Garnet.IssueTracking.Domain.Values;

namespace Garnet.IssueTracking.Operations.Commands
{
    public class UpdateIssueCommand : ICommand
    {
        public UpdateIssueCommand(long issueId, string title, string description, int trackerId, IssuePriority priority)
        {
            IssueId = issueId;
            Title = title;
            Description = description;
            TrackerId = trackerId;
            Priority = priority;
        }

        public long IssueId { get; }

        public string Title { get; }

        public string Description { get; }

        public int TrackerId { get; }

        public IssuePriority Priority { get; }
    }
}
