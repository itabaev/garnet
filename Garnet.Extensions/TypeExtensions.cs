﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Garnet.Extensions
{
    public static class TypeExtensions
    {
        private static IEnumerable<Type> GetBaseTypes(this Type type)
        {
            if (type == null)
                yield break;

            type = Nullable.GetUnderlyingType(type) ?? type;

            foreach (var interfaceType in type.GetInterfaces())
            {
                yield return interfaceType;
            }

            while (type != null)
            {
                yield return type;

                type = type.BaseType;
            }
        }

        public static bool IsInstanceOfGenericType<TInstance>(this TInstance instance, Type genericType, out Type foundedType)
        {
            if (!genericType.IsGenericType)
                throw new ArgumentException($"Type {genericType.Name} is not generic type");

            var instanceType = typeof(TInstance);
            var baseTypes = instanceType.GetBaseTypes();
            foundedType = baseTypes.FirstOrDefault(t => t.IsGenericType && t.GetGenericTypeDefinition() == genericType);
            return foundedType != null;
        }
    }
}
