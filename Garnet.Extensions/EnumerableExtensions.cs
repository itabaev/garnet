﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Garnet.Extensions
{
    public static class EnumerableExtensions
    {
        public static IReadOnlyCollection<T> ToReadOnly<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable is ReadOnlyCollection<T> readOnlyCollection)
                return readOnlyCollection;

            if (enumerable is List<T> list)
                return list.AsReadOnly();

            return enumerable.ToList().AsReadOnly();
        }
    }
}
