﻿using System.Threading.Tasks;
using Garnet.PersonnelManagement.Domain.Aggregates;

namespace Garnet.PersonnelManagement.Domain.Repositories
{
    public interface IPersonRepository
    {
        Task<Person> GetPerson(int personId);
    }
}