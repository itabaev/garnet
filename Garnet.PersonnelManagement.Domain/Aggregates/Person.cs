﻿using System;

namespace Garnet.PersonnelManagement.Domain.Aggregates
{
    public class Person
    {
        public Person(Guid personId, string firstName, string lastName)
        {
            PersonId = personId;
            FirstName = firstName;
            LastName = lastName;
        }

        public Guid PersonId { get; }

        public string FirstName { get; }

        public string LastName { get; }
    }
}
