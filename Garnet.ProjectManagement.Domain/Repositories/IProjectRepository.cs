﻿using System.Threading.Tasks;
using Garnet.ProjectManagement.Domain.Aggregates;

namespace Garnet.ProjectManagement.Domain.Repositories
{
    public interface IProjectRepository
    {
        Task<Project> GetProject(int projectId);
    }
}