﻿namespace Garnet.ProjectManagement.Domain.Aggregates
{
    public class Project
    {
        public Project(string projectId, string title)
        {
            ProjectId = projectId;
            Title = title;
        }

        public string ProjectId { get; }

        public string Title { get; }
    }
}