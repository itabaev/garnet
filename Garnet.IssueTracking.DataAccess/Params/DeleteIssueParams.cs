﻿namespace Garnet.IssueTracking.DataAccess.Params
{
    public class DeleteIssueParams
    {
        public DeleteIssueParams(long issueId)
        {
            IssueId = issueId;
        }

        public long IssueId { get; }
    }
}
