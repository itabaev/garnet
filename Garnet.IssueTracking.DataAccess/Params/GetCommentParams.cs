﻿namespace Garnet.IssueTracking.DataAccess.Params
{
    public class GetCommentParams
    {
        public GetCommentParams(long issueId, int commentId)
        {
            IssueId = issueId;
            CommentId = commentId;
        }

        public long IssueId { get; }

        public int CommentId { get; }
    }
}
