﻿namespace Garnet.IssueTracking.DataAccess.Params
{
    public class AddHistoryParams
    {
        public AddHistoryParams(long issueId, string historyType, string oldValue, string newValue)
        {
            IssueId = issueId;
            HistoryType = historyType;
            OldValue = oldValue;
            NewValue = newValue;
        }

        public long IssueId { get; }

        public string HistoryType { get; }

        public string OldValue { get; }

        public string NewValue { get; }
    }
}
