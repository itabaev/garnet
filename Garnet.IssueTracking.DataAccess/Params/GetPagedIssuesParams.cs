﻿namespace Garnet.IssueTracking.DataAccess.Params
{
    public class GetPagedIssuesParams
    {
        public GetPagedIssuesParams(int offset, int limit)
        {
            Offset = offset;
            Limit = limit;
        }

        public int Offset { get; }

        public int Limit { get; }
    }
}
