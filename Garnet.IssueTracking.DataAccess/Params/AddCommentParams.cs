﻿namespace Garnet.IssueTracking.DataAccess.Params
{
    public class AddCommentParams
    {
        public AddCommentParams(long issueId, string text)
        {
            IssueId = issueId;
            Text = text;
        }

        public long IssueId { get; }

        public string Text { get; }
    }
}
