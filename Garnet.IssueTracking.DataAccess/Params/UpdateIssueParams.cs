﻿namespace Garnet.IssueTracking.DataAccess.Params
{
    public class UpdateIssueParams
    {
        public UpdateIssueParams(long issueId, string title, string description, int trackerId, string priority, string state)
        {
            IssueId = issueId;
            Title = title;
            Description = description;
            TrackerId = trackerId;
            Priority = priority;
            State = state;
        }

        public long IssueId { get; }

        public string Title { get; }

        public string Description { get; }

        public int TrackerId { get; }

        public string Priority { get; }

        public string State { get; }
    }
}
