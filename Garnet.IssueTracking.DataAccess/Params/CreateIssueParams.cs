﻿using System;

namespace Garnet.IssueTracking.DataAccess.Params
{
    public class CreateIssueParams
    {
        public CreateIssueParams(string title, string description, int trackerId, string priority, string state, string projectId, Guid authorId)
        {
            Title = title;
            Description = description;
            TrackerId = trackerId;
            Priority = priority;
            State = state;
            ProjectId = projectId;
            AuthorId = authorId;
        }

        public string Title { get; }

        public string Description { get; }

        public int TrackerId { get; }

        public string Priority { get; }

        public string State { get; }

        public string ProjectId { get; }

        public Guid AuthorId { get; }
    }
}
