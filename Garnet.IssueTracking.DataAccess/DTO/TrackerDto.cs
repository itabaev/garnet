﻿namespace Garnet.IssueTracking.DataAccess.DTO
{
    public class TrackerDto
    {
        public int TrackerId { get; set; }

        public string Title { get; set; }
    }
}
