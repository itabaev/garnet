﻿namespace Garnet.IssueTracking.DataAccess.DTO
{
    public class IssuesCountDto
    {
        public int Count { get; set; }
    }
}
