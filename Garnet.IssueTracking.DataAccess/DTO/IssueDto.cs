﻿using System;

namespace Garnet.IssueTracking.DataAccess.DTO
{
    public class IssueDto
    {
        public long IssueId { get; set; }

        public DateTime CreateDate { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int TrackerId { get; set; }

        public string TrackerTitle { get; set; }

        public string Priority { get; set; }

        public string State { get; set; }
    }
}
