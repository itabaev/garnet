﻿using System;

namespace Garnet.IssueTracking.DataAccess.DTO
{
    public class HistoryDto
    {
        public DateTime Date { get; set; }

        public string HistoryType { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }
    }
}
