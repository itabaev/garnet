﻿using System;

namespace Garnet.IssueTracking.DataAccess.DTO
{
    public class CommentDto
    {
        public long CommentId { get; set; }

        public DateTime CreateDate { get; set; }

        public string Text { get; set; }
    }
}
