﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.DTO;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class GetIssuesTotalCountService : IDataAccessService<IssuesCountDto>
    {
        private const string ProcedureName = "issues.get_issues_count";

        private readonly ISqlSession _session;

        public GetIssuesTotalCountService(ISqlSession session)
        {
            _session = session;
        }

        public async Task<IssuesCountDto> Execute()
        {
            var connection = _session.GetConnection();
            var totalCount = await connection.ExecuteScalarAsync<int>(ProcedureName,
                null,
                commandType: CommandType.StoredProcedure);
            return new IssuesCountDto { Count = totalCount };
        }
    }
}
