﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.DataAccess.Extensions;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.Params;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class AddCommentService : IDataAccessService<int, AddCommentParams>
    {
        private const string ProcedureName = "issues.add_comment";

        private readonly ISqlSession _session;
        private readonly IDataParamsTransformer _paramsTransformer;

        public AddCommentService(ISqlSession session, IDataParamsTransformer paramsTransformer)
        {
            _session = session;
            _paramsTransformer = paramsTransformer;
        }

        public async Task<int> Execute(AddCommentParams @params)
        {
            var connection = _session.GetConnection();
            var commentId = await connection.ExecuteScalarAsync<int>(ProcedureName,
                param: _paramsTransformer.Transform(@params),
                commandType: CommandType.StoredProcedure);
            return commentId;
        }
    }
}
