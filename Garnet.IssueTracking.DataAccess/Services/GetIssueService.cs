﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.DTO;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class GetIssueService : IDataAccessService<IssueDto, long>
    {
        private const string ProcedureName = "issues.get_issue";

        private readonly ISqlSession _session;

        public GetIssueService(ISqlSession session)
        {
            _session = session;
        }

        public async Task<IssueDto> Execute(long issueId)
        {
            var connection = _session.GetConnection();
            var issue = await connection.QueryFirstOrDefaultAsync<IssueDto>(ProcedureName,
                new
                {
                    p_issue_id = issueId
                },
                commandType: CommandType.StoredProcedure);

            return issue;
        }
    }
}
