﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.DataAccess.Extensions;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.Params;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class CreateIssueService : IDataAccessService<long, CreateIssueParams>
    {
        private const string ProcedureName = "issues.create_issue";

        private readonly ISqlSession _session;
        private readonly IDataParamsTransformer _paramsTransformer;

        public CreateIssueService(ISqlSession session, IDataParamsTransformer paramsTransformer)
        {
            _session = session;
            _paramsTransformer = paramsTransformer;
        }

        public async Task<long> Execute(CreateIssueParams @params)
        {
            var connection = _session.GetConnection();
            var issueId = await connection.ExecuteScalarAsync<long>(ProcedureName,
                param: _paramsTransformer.Transform(@params),
                commandType: CommandType.StoredProcedure);
            return issueId;
        }
    }
}
