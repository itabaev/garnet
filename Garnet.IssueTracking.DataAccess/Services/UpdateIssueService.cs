﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.DataAccess.Extensions;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.Params;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class UpdateIssueService : IDataAccessService<bool, UpdateIssueParams>
    {
        private const string ProcedureName = "issues.update_issue";

        private readonly ISqlSession _session;
        private readonly IDataParamsTransformer _paramsTransformer;

        public UpdateIssueService(ISqlSession session, IDataParamsTransformer paramsTransformer)
        {
            _session = session;
            _paramsTransformer = paramsTransformer;
        }

        public async Task<bool> Execute(UpdateIssueParams @params)
        {
            var connection = _session.GetConnection();
            await connection.ExecuteAsync(ProcedureName,
                param: _paramsTransformer.Transform(@params),
                commandType: CommandType.StoredProcedure);
            return true;
        }
    }
}
