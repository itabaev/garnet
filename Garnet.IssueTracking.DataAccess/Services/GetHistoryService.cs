﻿using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.DTO;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class GetHistoryService : IDataAccessService<IEnumerable<HistoryDto>, long>
    {
        private const string ProcedureName = "issues.get_history";

        private readonly ISqlSession _session;

        public GetHistoryService(ISqlSession session)
        {
            _session = session;
        }

        public async Task<IEnumerable<HistoryDto>> Execute(long issueId)
        {
            var connection = _session.GetConnection();
            var history = await connection.QueryAsync<HistoryDto>(ProcedureName,
                new
                {
                    p_issue_id = issueId
                },
                commandType: CommandType.StoredProcedure);
            return history.ToList();
        }
    }
}
