﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.DTO;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class GetCommentsService : IDataAccessService<IEnumerable<CommentDto>, long>
    {
        private const string ProcedureName = "issues.get_comments";

        private readonly ISqlSession _session;

        public GetCommentsService(ISqlSession session)
        {
            _session = session;
        }

        public async Task<IEnumerable<CommentDto>> Execute(long issueId)
        {
            var connection = _session.GetConnection();
            var comments = await connection.QueryAsync<CommentDto>(ProcedureName,
                new
                {
                    p_issue_id = issueId
                },
                commandType: CommandType.StoredProcedure);

            return comments.ToList();
        }
    }
}
