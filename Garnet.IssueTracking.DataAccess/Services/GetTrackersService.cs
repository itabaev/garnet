﻿using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.DTO;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class GetTrackersService : IDataAccessService<IEnumerable<TrackerDto>>
    {
        private const string ProcedureName = "issues.get_trackers";

        private readonly ISqlSession _session;

        public GetTrackersService(ISqlSession session)
        {
            _session = session;
        }

        public async Task<IEnumerable<TrackerDto>> Execute()
        {
            var connection = _session.GetConnection();
            var trackers = await connection.QueryAsync<TrackerDto>(ProcedureName,
                commandType: CommandType.StoredProcedure);
            return trackers;
        }
    }
}
