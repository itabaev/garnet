﻿using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.DataAccess.Extensions;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.DataAccess.Params;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class GetPagedIssuesService : IDataAccessService<IEnumerable<IssueDto>, GetPagedIssuesParams>
    {
        private const string ProcedureName = "issues.get_paged_issues";

        private readonly ISqlSession _session;
        private readonly IDataParamsTransformer _paramsTransformer;

        public GetPagedIssuesService(ISqlSession session, IDataParamsTransformer paramsTransformer)
        {
            _session = session;
            _paramsTransformer = paramsTransformer;
        }

        public async Task<IEnumerable<IssueDto>> Execute(GetPagedIssuesParams @params)
        {
            var connection = _session.GetConnection();
            var issues = await connection.QueryAsync<IssueDto>(ProcedureName,
                param: _paramsTransformer.Transform(@params),
                commandType: CommandType.StoredProcedure);
            return issues.ToList();
        }
    }
}
