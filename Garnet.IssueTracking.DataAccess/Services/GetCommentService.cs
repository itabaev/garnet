﻿using System.Data;
using System.Threading.Tasks;
using Dapper;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.DataAccess.Extensions;
using Garnet.Infrastructure.SQL.Session;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.IssueTracking.DataAccess.Params;

namespace Garnet.IssueTracking.DataAccess.Services
{
    public class GetCommentService : IDataAccessService<CommentDto, GetCommentParams>
    {
        private const string ProcedureName = "issues.get_comment";

        private readonly ISqlSession _session;
        private readonly IDataParamsTransformer _paramsTransformer;

        public GetCommentService(ISqlSession session, IDataParamsTransformer paramsTransformer)
        {
            _session = session;
            _paramsTransformer = paramsTransformer;
        }

        public async Task<CommentDto> Execute(GetCommentParams @params)
        {
            var connection = _session.GetConnection();
            var comment = await connection.QueryFirstOrDefaultAsync<CommentDto>(ProcedureName,
                param: _paramsTransformer.Transform(@params),
                commandType: CommandType.StoredProcedure);
            return comment;
        }
    }
}
