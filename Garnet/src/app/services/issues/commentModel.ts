import { ITimeline } from './itimeline';

export class CommentModel implements ITimeline {
    commentId: number;
    createDate: Date;
    text: string;
    content: string;
    date: Date = this.createDate;
    type = 'Comment';

    constructor(commentId: number, createDate: Date, text: string, content: string) {
        this.commentId = commentId;
        this.date = this.createDate = createDate;
        this.text = text;
        this.content = content;
    }
}
