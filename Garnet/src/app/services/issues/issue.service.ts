import { IssueModel } from './issueModel';
import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ApiUrl } from '../../config';

import { IssuesModel } from 'src/app/services/issues/issuesModel';
import { TrackerModel } from 'src/app/services/issues/trackerModel';
import { HistoryModel } from 'src/app/services/issues/historyModel';
import { CommentModel } from 'src/app/services/issues/commentModel';

@Injectable({
  providedIn: 'root'
})
export class IssueService {
  private baseUrl: string;
  private issuesUrl = 'issues';
  private trackersUrl = 'issues/trackers';
  private historyUrl = 'history';
  private commentsUrl = 'comments';

  constructor(
    @Inject(ApiUrl) apiUrl: string,
    private http: HttpClient
  ) {
    this.baseUrl = apiUrl;
   }

  async getIssues(offset: number, limit: number): Promise<Observable<IssuesModel>> {
    const response = await this.http
      .get<IssuesModel>(
        this.baseUrl + this.issuesUrl,
        {
          headers: { 'Content-Type': 'application/json' },
          params: {
            offset: offset.toString(),
            limit: limit.toString()
          }
        })
      .toPromise();
    return of(response);
  }

  async getIssue(issueId: number): Promise<IssueModel> {
    const response = await this.http
      .get<IssueModel>(
        this.baseUrl + this.issuesUrl + '/' + issueId,
        {
          headers: { 'Content-Type': 'application/json' }
        })
      .toPromise();
    return response;
  }

  async updateIssue(issue: IssueModel): Promise<void> {
    const response = await this.http
      .put<void>(
        this.baseUrl + this.issuesUrl + '/' + issue.issueId,
        {
          title: issue.title,
          description: issue.description,
          trackerId: issue.tracker.trackerId,
          priority: issue.priority,
          state: issue.state
        },
        {
          headers: { 'Content-Type': 'application/json' }
        })
      .toPromise();
    return response;
  }

  async getTrackers(): Promise<TrackerModel[]> {
    const response = await this.http
      .get<TrackerModel[]>(
        this.baseUrl + this.trackersUrl,
        {
          headers: { 'Content-Type': 'application/json' }
        })
      .toPromise();
    return response;
  }

  async getHistory(issueId: number): Promise<HistoryModel[]> {
    const response = await this.http
      .get<HistoryModel[]>(
        this.baseUrl + this.issuesUrl + '/' + issueId + '/' + this.historyUrl,
        {
          headers: { 'Content-Type': 'application/json' }
        })
      .toPromise();
    return response;
  }

  async getComments(issueId: number): Promise<CommentModel[]> {
    const response = await this.http
      .get<CommentModel[]>(
        this.baseUrl + this.issuesUrl + '/' + issueId + '/' + this.commentsUrl,
        {
          headers: { 'Content-Type': 'application/json' }
        })
      .toPromise();
    return response;
  }

  async addComment(issueId: number, text: string): Promise<CommentModel> {
    const response = await this.http
      .post<CommentModel>(
        this.baseUrl + this.issuesUrl + '/' + issueId + '/' + this.commentsUrl,
        {
          text: text
        },
        {
          headers: { 'Content-Type': 'application/json' }
        })
      .toPromise();
    return response;
  }
}
