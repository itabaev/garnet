import { IssueModel } from './issueModel';

export class IssuesModel {
    issues: IssueModel[];
    totalCount: number;
}
