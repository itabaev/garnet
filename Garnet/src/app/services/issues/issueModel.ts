import { Person } from './person';
import { TrackerModel } from './trackerModel';
export class IssueModel {
    issueId: number;
    createDate: Date;
    title: string;
    description: string;
    tracker: TrackerModel;
    priority: string;
    state: string;
    author: Person;
}
