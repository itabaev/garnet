import { ITimeline } from './itimeline';

export class HistoryModel implements ITimeline {
    historyType: string;
    oldValue: string;
    newValue: string;
    date: Date;
    type = 'History';

    constructor(historyType: string, oldValue: string, newValue: string, date: Date) {
        this.historyType = historyType;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.date = date;
    }
}
