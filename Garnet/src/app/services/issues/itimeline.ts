export interface ITimeline {
    type: string;
    date: Date;
}
