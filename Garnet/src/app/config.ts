import { InjectionToken } from '@angular/core';
import { environment } from '../environments/environment';
export const ApiUrl = new InjectionToken<string>('api-url');
export const ApiUrlEnv = { provide: ApiUrl, useValue: environment.apiUrl };
