import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Converter } from 'showdown';
import * as ShowdownHtmlEscape from 'showdown-htmlescape';
import * as _ from 'underscore';

import { IssueService } from 'src/app/services/issues/issue.service';
import { IssueModel } from 'src/app/services/issues/issueModel';
import { HistoryModel } from 'src/app/services/issues/historyModel';
import { TrackerModel } from 'src/app/services/issues/trackerModel';
import { CommentModel } from 'src/app/services/issues/commentModel';
import { ITimeline } from 'src/app/services/issues/itimeline';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.scss']
})
export class IssueComponent implements OnInit {

  private converter: Converter;

  issueId: number;
  issue: IssueModel;
  content: string;
  timelines: object[];
  comment: string;
  preview: string;

  trackers: object = {};
  priorities: object = {
    'Low': 0,
    'Normal': 1,
    'High': 2,
    'Urgent': 3
  };

  constructor(
    private route: ActivatedRoute,
    private issueService: IssueService,
    private zone: NgZone
  ) {
    const classMap = {
      blockquote: 'blockquote'
    };
    const bindings = Object.keys(classMap)
      .map(key => ({
        type: 'output',
        regex: new RegExp(`<${key}>`, 'g'),
        replace: `<${key} class="${classMap[key]}">`
      }));
    this.converter = new Converter({
      extensions: [ShowdownHtmlEscape, bindings],
      noHeaderId: true,
      simpleLineBreaks: true,
      ghCodeBlocks: true,
      disableForced4SpacesIndentedSublists: true
    });
  }

  async ngOnInit() {
    this.issueId = +this.route.snapshot.paramMap.get('id');
    const trackers = await this.issueService.getTrackers();
    trackers.map(x => this.trackers[x.trackerId.toString()] = x.title);
    this.issue = await this.issueService.getIssue(this.issueId);
    this.content = this.converter.makeHtml(this.issue.description);
    const history = (await this.issueService.getHistory(this.issueId))
      .map(h => new HistoryModel(h.historyType, h.oldValue, h.newValue, h.date));
    const comments = (await this.issueService.getComments(this.issueId))
      .map(c => new CommentModel(c.commentId, c.createDate, c.text, this.converter.makeHtml(c.text)));
    let timelines = _.union<ITimeline>(history, comments);
    timelines = _.sortBy(timelines, o => o.date);
    this.timelines = timelines;
  }

  onCommentChanged() {
    this.zone.run(() => {
      this.preview = this.converter.makeHtml(this.comment);
    });
  }

  async addComment(): Promise<void> {
    const comment = await this.issueService.addComment(this.issueId, this.comment);
    this.timelines.push(new CommentModel(comment.commentId, comment.createDate, comment.text, this.converter.makeHtml(comment.text)));
  }
}
