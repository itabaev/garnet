import { async } from '@angular/core/testing';
import { Directive, Component, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription, Observable } from 'rxjs';

import { IssueModel } from 'src/app/services/issues/issueModel';
import { IssueService } from 'src/app/services/issues/issue.service';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.scss']
})
export class IssuesComponent implements OnInit, OnDestroy {
  pageSubscription: Subscription;
  itemsSubscription: Subscription;
  issues: IssueModel[];
  totalCount: number;
  currentPage = 1;
  currentItemsPerPage = 20;
  pageCount = 1;
  pages: number[] = [1];
  itemsList: number[] = [20, 50, 100];

  constructor(
    private route: ActivatedRoute,
    private issueService: IssueService
  ) { }

  async ngOnInit() {
    let page = +this.route.snapshot.queryParams['page'] || 1;
    if (page < 1) {
      page = 1;
    }
    this.currentPage = page;
    let items = +this.route.snapshot.queryParams['items'] || 1;
    if (items < 1) {
      items = 20;
    }
    this.currentItemsPerPage = items;
    await this.getIssues();

    this.pageSubscription = this.route
      .queryParams
      .subscribe(async params => {
        const newPage = +params['page'] || 1;
        await this.setPage(newPage);
      });

    this.itemsSubscription = this.route
      .queryParams
      .subscribe(async params => {
        const newItems = +params['items'] || 20;
        await this.setItems(newItems);
      });
  }

  ngOnDestroy() {
    if (this.pageSubscription != null) {
      this.pageSubscription.unsubscribe();
    }
    if (this.itemsSubscription != null) {
      this.itemsSubscription.unsubscribe();
    }
  }

  async setPage(page: number): Promise<void> {
    if (page < 1) {
      page = 1;
    }
    if (page > this.pageCount) {
      page = this.pageCount;
    }
    if (this.currentPage === page) {
      return;
    }
    this.currentPage = page;
    await this.getIssues();
  }

  async setItems(items: number): Promise<void> {
    if (items < 1) {
      items = 20;
    }
    if (this.currentItemsPerPage === items) {
      return;
    }
    this.currentItemsPerPage = items;
    await this.getIssues();
  }

  async getIssues(): Promise<void> {
    const currentPage = this.currentPage;
    const offset = (currentPage - 1) * this.currentItemsPerPage;
    const limit = this.currentItemsPerPage;
    const observable = await this.issueService.getIssues(offset, limit);
    observable.subscribe(data => {
      this.issues = data.issues;
      this.totalCount = data.totalCount;
      const pageCount = Math.ceil(data.totalCount / this.currentItemsPerPage);
      let pages = [];
      if (currentPage > 3) {
        pages.push(1);
      }
      if (currentPage > 4) {
        pages.push(null);
      }
      const aroundPagesFirst = Math.max(currentPage - 2, 1);
      const aroundPagesCount = Math.max(1, Math.min(pageCount, Math.min(pageCount - currentPage + 3, Math.min(currentPage + 2, 5))));
      const aroundPages = Array.from({ length: aroundPagesCount }, (value, key) => key + aroundPagesFirst);
      pages = pages.concat(aroundPages);
      if (currentPage < pageCount - 3) {
        pages.push(null);
      }
      if (currentPage < pageCount - 2) {
        pages.push(pageCount);
      }
      this.pageCount = pageCount;
      this.pages = pages;
    });
  }
}
