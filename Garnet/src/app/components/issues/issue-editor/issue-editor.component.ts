import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Converter } from 'showdown';
import * as ShowdownHtmlEscape from 'showdown-htmlescape';
import { IssueService } from 'src/app/services/issues/issue.service';
import { IssueModel } from 'src/app/services/issues/issueModel';
import { TrackerModel } from 'src/app/services/issues/trackerModel';

@Component({
  selector: 'app-issue-editor',
  templateUrl: './issue-editor.component.html',
  styleUrls: ['./issue-editor.component.scss']
})
export class IssueEditorComponent implements OnInit {
  private converter: Converter;

  issueId: number;
  issue: IssueModel;
  preview: string;
  trackers: TrackerModel[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private issueService: IssueService,
    private zone: NgZone
  ) {
    const classMap = {
      blockquote: 'blockquote'
    };
    const bindings = Object.keys(classMap)
      .map(key => ({
        type: 'output',
        regex: new RegExp(`<${key}>`, 'g'),
        replace: `<${key} class="${classMap[key]}">`
      }));
    this.converter = new Converter({
      extensions: [ShowdownHtmlEscape, bindings],
      noHeaderId: true,
      simpleLineBreaks: true,
      ghCodeBlocks: true,
      disableForced4SpacesIndentedSublists: true
    });
  }

  async ngOnInit() {
    this.issueId = +this.route.snapshot.paramMap.get('id');
    this.issue = await this.issueService.getIssue(this.issueId);
    this.trackers = await this.issueService.getTrackers();

    this.onDescriptionChanged();
  }

  onDescriptionChanged() {
    this.zone.run(() => {
      this.preview = this.converter.makeHtml(this.issue.description);
    });
  }

  compareIssueTracker(a: TrackerModel, b: TrackerModel): boolean {
    return a.trackerId === b.trackerId;
  }

  async saveChanges(): Promise<void> {
    await this.issueService.updateIssue(this.issue);
    this.router.navigate([ '/issues', this.issue.issueId ]);
  }
}
