import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { MomentModule } from 'angular2-moment';
import 'moment/locale/ru';

import { AppRoutingModule } from './modules/app-routing.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { IssuesComponent } from './components/issues/issues/issues.component';
import { IssueComponent } from './components/issues/issue/issue.component';
import { IssueEditorComponent } from './components/issues/issue-editor/issue-editor.component';

import { IssueService } from './services/issues/issue.service';

import { ApiUrlEnv } from './config';

registerLocaleData(localeRu);

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    IssuesComponent,
    IssueComponent,
    IssueEditorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    MomentModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'ru' },
    ApiUrlEnv,
    IssueService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
