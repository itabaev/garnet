﻿using System.Threading.Tasks;

namespace Garnet.Infrastructure.DataAccess
{
    public interface IDataAccessService
    {
        Task Execute();
    }

    public interface IDataAccessService<TResult>
    {
        Task<TResult> Execute();
    }

    public interface IDataAccessService<TResult, TParams>
    {
        Task<TResult> Execute(TParams @params);
    }
}
