﻿namespace Garnet.Infrastructure.DataAccess.Extensions
{
    public interface IDataParamsTransformer
    {
        object Transform(object obj);
    }
}
