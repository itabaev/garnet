create table issues.comment (
	issue_id		bigint 		not null,
	comment_id		int			not null,
	create_date		timestamp	not null,
	text			text		not null
)