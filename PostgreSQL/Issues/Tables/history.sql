create table issues.history (
	issue_id		bigint 		not null,
	date			timestamp	not null,
	history_type	varchar(16)	not null,
	old_value		text		null,
	new_value		text		null
)