create table issues.issue (
	issue_id	bigserial 		not null primary key,
	create_date	timestamp		not null,
	title		varchar(256)	not null,
	description	text			not null,
	tracker_id	int				not null references issues.tracker(tracker_id),
	priority	varchar(16)		not null,
    state       varchar(16)     not null
)