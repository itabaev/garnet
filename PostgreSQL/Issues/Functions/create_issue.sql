create or replace function issues.create_issue(
	in p_title			varchar(256),
	in p_description	text,
	in p_tracker_id		int,
	in p_priority		varchar(16),
	in p_state			varchar(16),
	in p_project_id 	varchar(32),
	in p_author_id		uuid
)
returns bigint
as $$
begin
	insert into issues.issue (create_date, title, description, tracker_id, priority, state)
	values (current_timestamp, p_title, p_description, p_tracker_id, p_priority, p_state);

	return currval(pg_get_serial_sequence('issues.issue', 'issue_id'));
end;
$$
language 'plpgsql';

alter function issues.create_issue(varchar(256), text, int, varchar(16), varchar(16), varchar(32), uuid)
    owner to pldzlxef;