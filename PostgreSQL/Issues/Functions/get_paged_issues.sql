create or replace function issues.get_paged_issues(
	in p_offset	int,
	in p_limit		int
)
returns table(
	issue_id		bigint,
	create_date		timestamp,
	title			varchar(256),
	description		text,
	tracker_id		int,
	tracker_title	varchar(128),
	priority		varchar(16),
	state			varchar(16)
)
as $$
begin
	return query
	select 	i.issue_id,
			i.create_date,
			i.title,
			i.description,
			tr.tracker_id,
			tr.title,
			i.priority,
			i.state
	from 	issues.issue i
	join	issues.tracker tr on tr.tracker_id = i.tracker_id
	order by i.issue_id desc
	limit 	p_limit
	offset 	p_offset;
end;
$$
language 'plpgsql';

alter function issues.get_paged_issues(int, int)
    owner to pldzlxef;