create or replace function issues.add_comment(
	in p_issue_id		bigint,
	in p_text			text
)
returns int
as $$
declare _comment_id int;
begin
	lock table issues.comment in share mode;

	insert into issues.comment(issue_id, comment_id, create_date, text)
	select	p_issue_id, coalesce(c.comment_id, 1), current_timestamp, p_text
	from	(
		select 	max(comment_id) + 1 as comment_id
		from 	issues.comment c
		where	c.issue_id = p_issue_id
	) c
	returning comment_id into _comment_id;
	
	return _comment_id;
end;
$$
language 'plpgsql';

alter function issues.add_comment(bigint, text)
    owner to pldzlxef;