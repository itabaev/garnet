create or replace function issues.get_comment(
	in p_issue_id 	bigint,
	in p_comment_id int
)
returns table(
	create_date		timestamp,
	text			text
)
as $$
begin
	return query
	select 	c.create_date,
			c.text
	from 	issues.issue i
	join	issues.comment c on c.issue_id = i.issue_id
	where 	i.issue_id = p_issue_id
		and c.comment_id = p_comment_id;
end;
$$
language 'plpgsql';

alter function issues.get_comment(bigint, int)
    owner to pldzlxef;