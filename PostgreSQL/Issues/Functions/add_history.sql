create or replace function issues.add_history(
	in p_issue_id		bigint,
	in p_history_type	varchar(16),
	in p_old_value		text,
	in p_new_value		text
)
returns void
as $$
begin
	insert into issues.history (issue_id, date, history_type, old_value, new_value)
	values (p_issue_id, current_timestamp, p_history_type, p_old_value, p_new_value);
end;
$$
language 'plpgsql';

alter function issues.add_history(bigint, varchar(16), text, text)
    owner to pldzlxef;