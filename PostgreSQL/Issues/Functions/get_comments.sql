create or replace function issues.get_comments(
	in p_issue_id	bigint
)
returns table(
	comment_id		int,
	create_date		timestamp,
	text			text
)
as $$
begin
	return query
	select 	c.comment_id,
			c.create_date,
			c.text
	from 	issues.issue i
	join	issues.comment c on c.issue_id = i.issue_id
	where 	i.issue_id = p_issue_id
	order by c.create_date asc;
end;
$$
language 'plpgsql';

alter function issues.get_comments(bigint)
    owner to pldzlxef;