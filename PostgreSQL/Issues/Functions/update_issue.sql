create or replace function issues.update_issue(
	in p_issue_id		bigint,
	in p_title			varchar(256),
	in p_description	text,
	in p_tracker_id		int,
	in p_priority		varchar(16),
	in p_state			varchar(16)
)
returns void
as $$
begin
	update	issues.issue
	set		title = p_title,
			description = p_description,
			tracker_id = p_tracker_id,
			priority = p_priority,
			state = p_state
	where	issue_id = p_issue_id;
end;
$$
language 'plpgsql';

alter function issues.update_issue(bigint, varchar(256), text, int, varchar(16), varchar(16))
    owner to pldzlxef;