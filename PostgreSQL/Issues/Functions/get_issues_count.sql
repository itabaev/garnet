create or replace function issues.get_issues_count()
returns int
as $$
begin
	return (
		select 	count(*)
		from 	issues.issue i
	);
end;
$$
language 'plpgsql';

alter function issues.get_issues_count()
    owner to pldzlxef;