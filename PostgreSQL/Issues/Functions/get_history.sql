create or replace function issues.get_history(in p_issue_id bigint)
returns table(
	date			timestamp,
	history_type	varchar(256),
	old_value		text,
	new_value		text
)
as $$
begin
	return query
	select 	h.date,
			h.history_type,
			h.old_value,
			h.new_value
	from 	issues.history h
	where	h.issue_id = p_issue_id
	order by h.date;
end;
$$
language 'plpgsql';

alter function issues.get_history(bigint)
    owner to pldzlxef;