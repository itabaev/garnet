create or replace function issues.get_trackers()
returns table(
	tracker_id	int,
	title		varchar(128)
)
as $$
begin
	return query
	select 	tr.tracker_id,
			tr.title
	from 	issues.tracker tr
	order by tr.title;
end;
$$
language 'plpgsql';

alter function issues.get_trackers()
    owner to pldzlxef;