﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Garnet.WebApi.Http.Exceptions;
using JetBrains.Annotations;

namespace Garnet.WebApi.Installers
{
    [UsedImplicitly]
    public class HandlersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes
                .FromAssemblyContaining<Startup>()
                .BasedOn(typeof(IExceptionHandler<>))
                .WithServiceBase()
                .LifestyleTransient());
        }
    }
}
