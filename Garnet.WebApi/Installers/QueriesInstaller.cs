﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Garnet.Infrastructure.Queries;
using Garnet.WebApi.Queries;
using JetBrains.Annotations;

namespace Garnet.WebApi.Installers
{
    [UsedImplicitly]
    public class QueriesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component
                .For<IQueryBuilder>()
                .ImplementedBy<QueryBuilder>()
                .LifestyleSingleton());

            container.Register(Classes
                .FromAssemblyInThisApplication(typeof(Startup).Assembly)
                .BasedOn(typeof(IQuery<>))
                .WithServiceFromInterface(typeof(IQuery<>))
                .LifestyleTransient());
        }
    }
}
