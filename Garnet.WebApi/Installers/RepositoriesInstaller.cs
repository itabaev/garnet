﻿using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.AspNetCore;
using Garnet.Infrastructure.Repositories;
using Garnet.IssueTracking.Domain.Aggregates;
using Garnet.IssueTracking.Domain.Dapper.Repositories;
using Garnet.IssueTracking.Domain.Repositories;

namespace Garnet.WebApi.Installers
{
    public class RepositoriesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component
                .For(typeof(IIssueRepository), typeof(IRepository<Issue>), typeof(IRepository<Issue, long>))
                .ImplementedBy<IssueRepository>()
                .LifestylePerWebRequest());

            container.Register(Component
                .For<IRepositoryFactory>()
                .AsFactory()
                .LifestyleSingleton());
        }
    }
}
