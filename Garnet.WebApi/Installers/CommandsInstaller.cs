﻿using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.AspNetCore;
using Garnet.Infrastructure.Commands;
using Garnet.WebApi.Commands;

namespace Garnet.WebApi.Installers
{
    public class CommandsInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component
                .For<ICommandProcessor>()
                .ImplementedBy<CommandProcessor>()
                .LifestylePerWebRequest());

            container.Register(Component
                .For<ICommandHandlerFactory>()
                .AsFactory()
                .LifestyleSingleton());

            container.Register(Classes
                .FromAssemblyInThisApplication(typeof(Startup).Assembly)
                .BasedOn(typeof(ICommandHandler<>), typeof(ICommandHandler<,>))
                .WithServiceBase()
                .LifestyleTransient());
        }
    }
}
