﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.AspNetCore;
using Garnet.Infrastructure.DataAccess;
using Garnet.Infrastructure.DataAccess.Extensions;
using Garnet.WebApi.Configuration.Transforms;

namespace Garnet.WebApi.Installers
{
    public class ServicesInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component
                .For<IDataParamsTransformer>()
                .ImplementedBy<DataParamsPropertyTransformer>()
                .LifestyleSingleton());

            container.Register(Classes
                .FromAssemblyInThisApplication(typeof(Startup).Assembly)
                .BasedOn(typeof(IDataAccessService), typeof(IDataAccessService<>), typeof(IDataAccessService<,>))
                .WithServiceBase()
                .LifestylePerWebRequest());
        }
    }
}
