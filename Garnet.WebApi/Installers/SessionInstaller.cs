﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Castle.Windsor.AspNetCore;
using Garnet.Infrastructure.PostgreSQL.Session;
using Garnet.Infrastructure.SQL.Session;
using Garnet.Infrastructure.SQL.UnitOfWork;
using Garnet.Infrastructure.UnitOfWork;
using Garnet.WebApi.Configuration;
using JetBrains.Annotations;
using Microsoft.Extensions.Options;

namespace Garnet.WebApi.Installers
{
    [UsedImplicitly]
    public class SessionInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component
                .For<ISqlSessionFactory>()
                .UsingFactoryMethod(kernel =>
                {
                    var connectionString = kernel.Resolve<IOptions<ConnectionStringsConfig>>().Value.Default;
                    return new SqlSessionFactory(connectionString);
                })
                .LifestyleSingleton());

            container.Register(Component
                .For<ISqlSession>()
                .UsingFactoryMethod(kernel =>
                {
                    var factory = kernel.Resolve<ISqlSessionFactory>();
                    return factory.Create();
                })
                .LifestylePerWebRequest());

            container.Register(Component
                .For<IUnitOfWorkFactory>()
                .ImplementedBy<SqlUnitOfWorkFactory>()
                .LifestylePerWebRequest());
        }
    }
}
