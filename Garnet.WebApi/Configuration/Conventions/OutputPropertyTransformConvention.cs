﻿using Dapper.FluentMap.Conventions;
using System.Text.RegularExpressions;

namespace Garnet.WebApi.Configuration.Conventions
{
    public class OutputPropertyTransformConvention : Convention
    {
        public OutputPropertyTransformConvention()
        {
            Properties()
                .Configure(c =>
                    c.Transform(s =>
                        Regex
                            .Replace(input: s, pattern: @"([a-z\d])([A-Z])", replacement: "$1_$2")
                            .ToLower()
                    )
                );
        }
    }
}
