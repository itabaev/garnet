﻿using Garnet.Infrastructure.DataAccess.Extensions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Garnet.WebApi.Configuration.Transforms
{
    public class DataParamsPropertyTransformer : IDataParamsTransformer
    {
        private readonly MemoryCache _cache;
        private readonly Regex _regex;

        public DataParamsPropertyTransformer()
        {
            _cache = new MemoryCache(Options.Create(new MemoryCacheOptions()));
            _regex = new Regex(@"([a-z\d])([A-Z])");
        }

        public object Transform(object obj)
        {
            if (obj == null)
                return null;

            var properties = GetTypeProperties(obj.GetType());
            var result = new ExpandoObject() as IDictionary<string, object>;
            foreach (var prop in properties)
            {
                var name = "p_" + _regex.Replace(prop.Name, "$1_$2").ToLower();
                var value = prop.GetValue(obj);
                result.Add(name, value);
            }

            return result;
        }

        private PropertyInfo[] GetTypeProperties(Type type)
        {
            var key = type.FullName;
            if (!_cache.TryGetValue<PropertyInfo[]>(key, out var properties))
            {
                properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
                _cache.Set(key, properties);
            }
            return properties;
        }
    }
}
