﻿namespace Garnet.WebApi.Configuration
{
    public class CorsConfig
    {
        public string[] Origins { get; set; }
    }
}
