﻿namespace Garnet.WebApi.Models.IssueTracking
{
    public class AddCommentModel
    {
        public string Text { get; set; }
    }
}
