﻿using Garnet.IssueTracking.Domain.Values;

namespace Garnet.WebApi.Models.IssueTracking
{
    public class UpdateIssueModel
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public int TrackerId { get; set; }

        public IssuePriority Priority { get; set; }
    }
}
