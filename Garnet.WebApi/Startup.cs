﻿using System;
using Castle.Facilities.TypedFactory;
using Castle.Windsor;
using Castle.Windsor.AspNetCore;
using Castle.Windsor.Installer;
using Dapper.FluentMap;
using Garnet.IssueTracking.DataAccess.DTO;
using Garnet.WebApi.Configuration;
using Garnet.WebApi.Configuration.Conventions;
using Garnet.WebApi.Http.Filters;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Garnet.WebApi
{
    public class Startup
    {
        private const string CorsPolicyName = "AllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        [UsedImplicitly]
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();

            services.AddCors(options =>
            {
                var corsConfig = Configuration.GetSection("Cors").Get<CorsConfig>();

                options.AddPolicy(CorsPolicyName,
                    builder => builder
                        .WithOrigins(corsConfig.Origins)
                        .AllowAnyHeader()
                        .AllowAnyMethod());
            });

            services
                .AddMvcCore(options =>
                {
                    options.Filters.Add(new CorsAuthorizationFilterFactory(CorsPolicyName));
                    options.Filters.Add<ExceptionFilter>();
                })
                .AddFormatterMappings()
                .AddJsonFormatters()
                .AddCors();

            services.Configure<ConnectionStringsConfig>(Configuration.GetSection("ConnectionStrings"));

            FluentMapper.Initialize(config =>
            {
                config.AddConvention<OutputPropertyTransformConvention>()
                    .ForEntitiesInAssembly(typeof(IssueDto).Assembly, typeof(IssueDto).Namespace);
            });

            var container = new WindsorContainer();
            container.AddFacility<TypedFactoryFacility>();
            container.Install(FromAssembly.Containing<Startup>());
            return WindsorRegistrationHelper.CreateServiceProvider(container, services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        [UsedImplicitly]
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(CorsPolicyName);

            app.UseMvc();
        }
    }
}
