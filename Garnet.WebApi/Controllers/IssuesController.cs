﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Garnet.Infrastructure.Commands;
using Garnet.Infrastructure.Queries;
using Garnet.IssueTracking.Operations.Commands;
using Garnet.IssueTracking.Operations.Queries.Criteria;
using Garnet.IssueTracking.Operations.Queries.Models;
using Garnet.WebApi.Exceptions.Issues;
using Garnet.WebApi.Models.IssueTracking;
using Microsoft.AspNetCore.Mvc;

namespace Garnet.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class IssuesController : ControllerBase
    {
        public IssuesController(IQueryBuilder query, ICommandProcessor command)
        {
            Query = query;
            Command = command;
        }

        public IQueryBuilder Query { get; }

        public ICommandProcessor Command { get; }

        // GET: api/issues
        [HttpGet]
        public async Task<object> Get(int offset = 0, int limit = 20)
        {
            if (offset < 0) throw new ArgumentOutOfRangeException(nameof(offset));
            if (limit <= 0) throw new ArgumentOutOfRangeException(nameof(limit));

            var criterion = new FindPagedIssuesCriterion(offset, limit);
            var issues = await Query.For<IssueModel>().AllWith(criterion);
            var totalCount = await Query.For<int>().OneWith<FindIssuesCriterion>(criterion);

            return new { issues = issues.ToList(), totalCount };
        }

        // GET: api/issues/5
        [HttpGet("{issueId:long}")]
        public async Task<IssueModel> Get(long issueId)
        {
            var issue = await Query.For<IssueModel>().One(issueId);
            if (issue == null) throw new IssueNotFoundException(issueId);

            return issue;
        }

        // POST: api/issues
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/issues/5
        [HttpPut("{issueId:long}")]
        public async Task<bool> Put(long issueId, [FromBody]UpdateIssueModel model)
        {
            var command = new UpdateIssueCommand(issueId, model.Title, model.Description, model.TrackerId, model.Priority);
            await Command.Handle(command);

            return true;
        }

        // DELETE: api/issues/5
        [HttpDelete("{issueId:long}")]
        public void Delete(long issueId)
        {
        }

        [HttpGet("trackers")]
        public async Task<IEnumerable<TrackerModel>> GetTrackers()
        {
            var trackers = await Query.For<TrackerModel>().All();
            return trackers.ToList();
        }

        [HttpGet("{issueId:long}/history")]
        public async Task<IEnumerable<HistoryModel>> GetHistory(long issueId)
        {
            var criterion = new FindByIssueIdCriterion(issueId);
            var history = await Query.For<HistoryModel>().AllWith(criterion);
            return history.ToList();
        }
    }
}
