﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Garnet.Infrastructure.Commands;
using Garnet.Infrastructure.Queries;
using Garnet.IssueTracking.Operations.Commands;
using Garnet.IssueTracking.Operations.Queries.Criteria;
using Garnet.IssueTracking.Operations.Queries.Models;
using Garnet.WebApi.Exceptions.Issues;
using Garnet.WebApi.Models.IssueTracking;
using Microsoft.AspNetCore.Mvc;

namespace Garnet.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/issues/{issueId:long}/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        public CommentsController(IQueryBuilder query, ICommandProcessor command)
        {
            Query = query;
            Command = command;
        }

        public IQueryBuilder Query { get; }

        public ICommandProcessor Command { get; }

        // GET: api/issues/2/comments
        [HttpGet]
        public async Task<IEnumerable<CommentModel>> Get(long issueId)
        {
            var comments = await Query.For<CommentModel>().AllWith(new FindByIssueIdCriterion(issueId));
            return comments.ToList();
        }

        // GET: api/issues/2/comments/5
        [HttpGet("{commentId:int}")]
        public async Task<CommentModel> Get(long issueId, int commentId)
        {
            var comment = await Query.For<CommentModel>().OneWith(new FindByCommentIdCriterion(issueId, commentId));
            if (comment == null) throw new CommentNotFoundException(issueId, commentId);

            return comment;
        }

        // POST: api/issues/2/comments
        [HttpPost]
        public async Task<CommentModel> Post(long issueId, [FromBody]AddCommentModel model)
        {
            var command = new AddCommentCommand(issueId, model.Text);
            var commentId = await Command.Handle<AddCommentCommand, int>(command);
            var comment = await Query.For<CommentModel>().OneWith(new FindByCommentIdCriterion(issueId, commentId));
            return comment;
        }

        // PUT: api/issues/2/comments/5
        [HttpPut("{commentId:int}")]
        public void Put(long issueId, int commentId, [FromBody]string value)
        {
        }

        // DELETE: api/issues/2/comments/5
        [HttpDelete("{commentId:int}")]
        public void Delete(long issueId, int commentId)
        {
        }
    }
}
