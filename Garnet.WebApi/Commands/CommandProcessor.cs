﻿using Garnet.Infrastructure.Commands;
using System.Threading.Tasks;

namespace Garnet.WebApi.Commands
{
    public class CommandProcessor : ICommandProcessor
    {
        private readonly ICommandHandlerFactory _commandHandlerFactory;

        public CommandProcessor(ICommandHandlerFactory commandHandlerFactory)
        {
            _commandHandlerFactory = commandHandlerFactory;
        }

        public async Task Handle<TCommand>(TCommand command)
            where TCommand : ICommand
        {
            var commandHandler = _commandHandlerFactory.Create<TCommand>();
            await commandHandler.Handle(command);
        }

        public async Task<TResult> Handle<TCommand, TResult>(TCommand command)
            where TCommand : ICommand
        {
            var commandHandler = _commandHandlerFactory.Create<TCommand, TResult>();
            return await commandHandler.Handle(command);
        }
    }
}
