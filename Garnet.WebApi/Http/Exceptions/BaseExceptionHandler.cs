﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Garnet.WebApi.Http.Exceptions
{
    public abstract class BaseExceptionHandler<T> : IExceptionHandler<T>
        where T : Exception
    {
        public Task<IActionResult> Handle(Exception exception)
        {
            if (!(exception is T typedException))
                throw new ArgumentException($"Wrong exception type {exception.GetType().Name}");

            return Task.FromResult(Handle(typedException));
        }

        protected abstract IActionResult Handle(T exception);
    }
}
