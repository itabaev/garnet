﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Garnet.WebApi.Http.Exceptions
{
    public interface IExceptionHandler
    {
        Task<IActionResult> Handle(Exception exception);
    }

    public interface IExceptionHandler<in T> : IExceptionHandler
        where T : Exception
    {
    }
}