﻿using Garnet.WebApi.Exceptions;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;

namespace Garnet.WebApi.Http.Exceptions
{
    [UsedImplicitly]
    public class ResourceNotFoundExceptionHandler : BaseExceptionHandler<ResourceNotFoundException>
    {
        protected override IActionResult Handle(ResourceNotFoundException exception)
        {
            return new NotFoundResult();
        }
    }
}
