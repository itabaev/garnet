﻿using System;
using System.Threading.Tasks;
using Garnet.WebApi.Http.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Garnet.WebApi.Http.Filters
{
    public class ExceptionFilter : IAsyncExceptionFilter
    {
        private readonly IServiceProvider _serviceProvider;

        public ExceptionFilter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task OnExceptionAsync(ExceptionContext context)
        {
            var handler = GetHandler(context.Exception.GetType());

            if (handler != null)
            {
                context.Result = await handler.Handle(context.Exception);
            }
            else
            {
                context.Result = new StatusCodeResult(500);
            }
        }

        private IExceptionHandler GetHandler(Type exceptionType, int deep = 0)
        {
            var handlerType = typeof(IExceptionHandler<>).MakeGenericType(exceptionType);
            var handler = (IExceptionHandler)_serviceProvider.GetService(handlerType);

            if (handler == null && deep < 1)
                return GetHandler(exceptionType.BaseType, deep + 1);

            return handler;
        }
    }
}
