﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Garnet.Infrastructure.Queries;

namespace Garnet.WebApi.Queries
{
    public class QueryBuilder : IQueryBuilder
    {
        private readonly IServiceProvider _serviceProvider;

        public QueryBuilder(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IQueryFor<TResult> For<TResult>()
        {
            return new QueryFor<TResult>(_serviceProvider);
        }

        #region Nested type: QueryFor

        private class QueryFor<TResult> : IQueryFor<TResult>
        {
            private readonly IServiceProvider _serviceProvider;

            public QueryFor(IServiceProvider serviceProvider)
            {
                _serviceProvider = serviceProvider;
            }

            public Task<IEnumerable<TResult>> All()
            {
                return _serviceProvider.GetRequiredService<IFindAllQuery<TResult>>().Execute();
            }

            public Task<IEnumerable<TResult>> AllWith<TCriterion>(TCriterion criterion) where TCriterion : ICriterion
            {
                return _serviceProvider.GetRequiredService<IFindAllWithCriterionQuery<TCriterion, TResult>>().Execute(criterion);
            }

            public Task<TResult> OneWith<TCriterion>(TCriterion criterion) where TCriterion : ICriterion
            {
                return _serviceProvider.GetRequiredService<IFindWithCriterionQuery<TCriterion, TResult>>().Execute(criterion);
            }

            public Task<TResult> One<TId>(TId id) where TId : struct
            {
                return _serviceProvider.GetRequiredService<IFindByIdQuery<TId, TResult>>().Execute(id);
            }
        }

        #endregion
    }
}
