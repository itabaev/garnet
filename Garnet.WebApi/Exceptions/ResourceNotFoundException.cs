﻿using System;

namespace Garnet.WebApi.Exceptions
{
    public abstract class ResourceNotFoundException : Exception
    {
        public ResourceNotFoundException(object value)
        {
            Value = value;
        }

        public ResourceNotFoundException(object value, string message) : base(message)
        {
            Value = value;
        }

        public ResourceNotFoundException(object value, string message, Exception innerException) : base(message, innerException)
        {
            Value = value;
        }

        public object Value { get; }
    }
}
