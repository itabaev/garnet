﻿using System;

namespace Garnet.WebApi.Exceptions.Issues
{
    public class IssueNotFoundException : ResourceNotFoundException
    {
        public IssueNotFoundException(long issueId)
            : this(issueId, null)
        {
        }

        public IssueNotFoundException(long issueId, Exception innerException)
            : base(issueId, $"Issue with id {issueId} not found", innerException)
        {
            IssueId = issueId;
        }

        public long IssueId { get; }
    }
}
