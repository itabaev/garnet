﻿using System;

namespace Garnet.WebApi.Exceptions.Issues
{
    public class CommentNotFoundException : ResourceNotFoundException
    {
        public CommentNotFoundException(long issueId, int commentId)
            : this(issueId, commentId, null)
        {
        }

        public CommentNotFoundException(long issueId, int commentId, Exception innerException)
            : base($"{issueId}:{commentId}", $"Comment with id {commentId} for issue with id {issueId} not found", innerException)
        {
            IssueId = issueId;
            CommentId = commentId;
        }

        public long IssueId { get; }

        public int CommentId { get; }
    }
}
