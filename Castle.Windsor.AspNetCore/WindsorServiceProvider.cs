﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;

namespace Castle.Windsor.AspNetCore
{
    /// <summary>
    /// Implements <see cref="IServiceProvider"/>.
    /// </summary>
    public class WindsorServiceProvider : IServiceProvider, ISupportRequiredService
    {
        private readonly IWindsorContainer _container;

        public bool IsInResolving
        {
            get { return _isInResolving.Value; }
            set { _isInResolving.Value = value; }
        }

        private readonly AsyncLocal<bool> _isInResolving = new AsyncLocal<bool>();

        public WindsorServiceProvider(IWindsorContainer container)
        {
            _container = container;
        }

        public object GetService(Type serviceType)
        {
            return GetServiceInternal(serviceType, true);
        }

        public object GetRequiredService(Type serviceType)
        {
            return GetServiceInternal(serviceType, false);
        }

        private object GetServiceInternal(Type serviceType, bool isOptional)
        {
            var isAlreadyInResolving = IsInResolving;

            if (!isAlreadyInResolving)
            {
                IsInResolving = true;
            }

            try
            {
                return ResolveInstanceOrNull(serviceType, isOptional);
            }
            finally
            {
                if (!isAlreadyInResolving)
                {
                    IsInResolving = false;
                }
            }
        }

        private object ResolveInstanceOrNull(Type serviceType, bool isOptional)
        {
            //Check if given service is directly registered
            if (_container.Kernel.HasComponent(serviceType))
            {
                return _container.Resolve(serviceType);
            }

            // Check if requested IEnumerable<TService>
            // MS uses GetService<IEnumerable<TService>>() to get a collection.
            // This must be resolved with IWindsorContainer.ResolveAll();

            if (serviceType.GetTypeInfo().IsGenericType && serviceType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
            {
                var allObjects = _container.ResolveAll(serviceType.GenericTypeArguments[0]);
                Array.Reverse(allObjects);
                return allObjects;
            }

            if (isOptional)
            {
                //Not found
                return null;
            }

            //Let Castle Windsor throws exception since the service is not registered!
            return _container.Resolve(serviceType);
        }
    }
}