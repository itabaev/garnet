﻿using System;

namespace Castle.Windsor.AspNetCore
{
    public static class GlobalWindsorServiceProvider
    {
        public static IServiceProvider Instance { get; set; }
    }
}
