﻿using Castle.MicroKernel.Registration;
using Castle.Windsor.AspNetCore.Lifestyles;

namespace Castle.Windsor.AspNetCore
{
    public static class ComponentRegistrationExtensions
    {
        public static ComponentRegistration<T> LifestylePerWebRequest<T>(this ComponentRegistration<T> registration)
            where T : class
        {
            return registration.LifeStyle.Scoped<WebRequestScopeAccessor>();
        }

        public static BasedOnDescriptor LifestylePerWebRequest(this BasedOnDescriptor registration)
        {
            return registration.LifestyleScoped<WebRequestScopeAccessor>();
        }
    }
}
