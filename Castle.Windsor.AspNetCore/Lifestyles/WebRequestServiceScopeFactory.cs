﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Castle.Windsor.AspNetCore.Lifestyles
{
    public class WebRequestServiceScopeFactory : IServiceScopeFactory
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public WebRequestServiceScopeFactory(IServiceProvider serviceProvider, IHttpContextAccessor httpContextAccessor)
        {
            _serviceProvider = serviceProvider;
            _httpContextAccessor = httpContextAccessor;
        }

        public IServiceScope CreateScope()
        {
            return new WebRequestServiceScope(_serviceProvider, _httpContextAccessor);
        }
    }
}
