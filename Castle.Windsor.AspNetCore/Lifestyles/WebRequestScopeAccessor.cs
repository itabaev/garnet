﻿using Castle.MicroKernel.Context;
using Castle.MicroKernel.Lifestyle.Scoped;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Castle.Windsor.AspNetCore.Lifestyles
{
    public class WebRequestScopeAccessor : IScopeAccessor
    {
        private const string LifetimeScopeKey = "castle.per-web-request-lifestyle-cache";

        public ILifetimeScope GetScope(CreationContext context)
        {
            var serviceProvider = GlobalWindsorServiceProvider.Instance;
            var httpContext = serviceProvider?.GetService<IHttpContextAccessor>().HttpContext;
            if (httpContext == null)
                return null;

            httpContext.Items.TryGetValue(LifetimeScopeKey, out var lifetimeScope);
            return lifetimeScope as ILifetimeScope;
        }

        public void Dispose()
        {
        }
    }
}
