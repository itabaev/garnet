﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Castle.Windsor.AspNetCore.Lifestyles
{
    public class WebRequestServiceScope : IServiceScope
    {
        private const string LifetimeScopeKey = "castle.per-web-request-lifestyle-cache";

        private readonly WebRequestLifetimeScope _lifetimeScope;

        public WebRequestServiceScope(IServiceProvider serviceProvider, IHttpContextAccessor httpContextAccessor)
        {
            ServiceProvider = serviceProvider;

            var httpContext = httpContextAccessor.HttpContext;
            if (httpContext != null)
            {
                _lifetimeScope = new WebRequestLifetimeScope();
                httpContext.Items.Add(LifetimeScopeKey, _lifetimeScope);
            }
        }

        public IServiceProvider ServiceProvider { get; }

        public void Dispose()
        {
            _lifetimeScope?.Dispose();
        }
    }
}
